import { Card } from "@mui/material";
import React, { Component } from "react";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import { withRouter, Route, Router, Routes } from "react-router-dom";
import App from "./App";
import ContactEmployee from "./Components/pages/ContactEmployee";
import Dashboard from "./Dashboard";
import EventCalender from "./Components/pages/EventCalender";
import { createBrowserHistory } from "history";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserName: "",
      Password: "",
    };
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("https://localhost:44325/api/login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          UserName: this.state.UserName,
          Password: this.state.Password,
        }),
      });

      if (response.ok) {
        this.props.history.push("/dashboard");
      } else {
        console.log("Invalid username or password");
        // Handle unsuccessful login
      }
    } catch (error) {
      console.error(error);
    }
  };

  // handleChange = (e) => {
  //   this.setState({ [e.target.name]: e.target.value });
  // };

  changePassword = (e) => {
    this.setState({ Password: e.target.value });
  };
  changeUserName = (e) => {
    this.setState({ UserName: e.target.value });
  };

  render() {
    return (
      <Card style={{ marginTop: 60, padding: 10, borderRadius: 25 }}>
        {/* <BrowserRouter>
          <Routes>
            <Route
              path="/Employee/Contact"
              render={(props) => <ContactEmployee {...props} />}
            />
            <Route path="/EventCalander" element={<EventCalender />} />
          </Routes>
        </BrowserRouter> */}
        <div className="m-4 " style={{ alignItems: "Block", display: "Block" }}>
          <div className="col-6" style={{ width: 400 }}>
            <div className="input-group ">
              <span className="input-group-text">
                <PersonOutlineIcon />
              </span>

              <input
                type="text"
                className="form-control"
                placeholder="Username"
                value={this.state.UserName}
                onChange={this.changeUserName}
              />
            </div>
          </div>
          <div className="col-6" style={{ width: 400 }}>
            <div className="input-group">
              <span className="input-group-text">
                <PersonOutlineIcon />
              </span>

              <input
                type="text"
                className="form-control"
                placeholder="Password"
                value={this.state.Password}
                onChange={this.changePassword}
              />
            </div>
          </div>
        </div>
        <button type="submit" onClick={this.handleSubmit}>
          Login
        </button>
      </Card>
    );
  }
}

export default withRouter(Login);
