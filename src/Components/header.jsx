import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import logo from "../assets/logo-1 (1).png";
import "./CssFiles/custom.css";

const pages = [""];
const settings = ["Profile", "Logout"];

const ResponsiveAppBar = () => {
  return (
    <AppBar
      position="static"
      style={{
        position: "fixed",
        zIndex: 9999,
        marginTop: -10,
        top: 0,
      }}
    >
      <Container className="headertopbar" maxWidth="xxl">
        <Toolbar className="headertopbar" disableGutters>
          <Typography sx={{ color: "white", weight: 2, type: "bold" }}>
            Employee Management
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
