import {
  Button,
  Card,
  CardHeader,
  Grid,
  Paper,
  Typography,
} from "@mui/material";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import MultiActionAreaCard from "../Admin/subcomponents/LeaveManagement/LeaveCardview";

import { experimentalStyled as styled } from "@mui/material/styles";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function LeaveRequest() {
  return (
    <div>
      <Card className="card">
        <CardHeader
          title={
            <Typography className="cardHeader">My Leave Requests</Typography>
          }
        />
        <Link to="/">
          <Button
            variant="contained"
            style={{
              borderRadius: 10,
              backgroundColor: "#21b6ae",
              padding: "12px 36px",
              fontSize: "12px",
              margin: "20px",
            }}
          >
            Back
          </Button>
        </Link>

        <Grid
          container
          spacing={{ xs: 1, md: 1 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {Array.from(Array(6)).map((_, index) => (
            <Grid item xs={2} sm={4} md={2} key={index}>
              <Item>
                {" "}
                <MultiActionAreaCard />{" "}
              </Item>
            </Grid>
          ))}
        </Grid>
      </Card>
    </div>
  );
}
