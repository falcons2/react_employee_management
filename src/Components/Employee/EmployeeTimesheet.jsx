import { Button, Card, CardHeader, Typography } from '@mui/material'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class EmployeeTimesheet extends Component {
  render() {
    return (
      <div>
         <Card className='card'>
         <CardHeader
          title={<Typography className='cardHeader' >Timesheet </Typography>}
          />
          <div><br></br></div>
          <Link to="/">
          <Button  variant="contained" className='backbtn' style={{
         borderRadius: 10,
         backgroundColor: "#21b6ae",
         padding: "12px 36px",
         fontSize: "12px",
         margin: "20px"}}>Back</Button>
          </Link>

          <h1>my timesheet</h1>
          </Card>
          </div>
    )
  }
}
