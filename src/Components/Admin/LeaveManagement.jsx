import React, { Component } from "react";
import "devextreme/dist/css/dx.light.css";
import "../CssFiles/custom.css";

import { Button, Card, CardHeader, Typography } from "@mui/material";
import { Link } from "react-router-dom";

export default function LeaveManagement() {
  return (
    <div>
      <Card className="card">
        <CardHeader
          title={
            <Typography className="cardHeader">Leave Management </Typography>
          }
        />
        <Link to="/">
          <Button
            variant="contained"
            style={{
              borderRadius: 10,
              backgroundColor: "#21b6ae",
              padding: "12px 36px",
              fontSize: "12px",
              margin: "20px",
            }}
          >
            Back
          </Button>
        </Link>
      </Card>
    </div>
  );
}
