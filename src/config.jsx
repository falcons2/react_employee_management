const TEST_URL = ``; //Test API
const LIVE_URL = ``; //LIVE API
const LOCAL_URL = `https://localhost:44325/api/`;

const ENV = 0; // 0 = dev, 1 = test, 2 = live
const VERSION = "1.0.0";
const PAGE_SIZE = 10;
let API_URL = "https://localhost:44325/api/";

if (ENV == 1) {
  API_URL = TEST_URL;
} else if (ENV == 2) {
  API_URL = LIVE_URL;
  //   API_URL = config.API_URL;
} else {
  API_URL = LOCAL_URL;
}

const COLOURS = [
  "#337AB7", //Primary Blue
  "#a5378b", //Secondary Purple
  "#E8E8E8", //Background grey
  "#c1c1c1", //Disabled
];

const HOVER_COLOR = "#c1c1c1";

export { API_URL, ENV, COLOURS, HOVER_COLOR, VERSION, PAGE_SIZE };
