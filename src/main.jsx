import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "./App";
import Dashboard from "./Dashboard";
import "./index.css";
import Login from "./Login";

ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <Login />
  </BrowserRouter>
);

//     {/* <Dashboard /> */}
//     {/* <App /> */}
// <React.StrictMode>
// <Login />
//   </React.StrictMode>
